/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.infrastructure.component;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * The resolver of {@code WebElement}S for component abstractions. Provides instances upon access, based on a variety of
 * element selectors.
 * <p>
 * Each component abstraction internally operates on {@code WebElements} for best flexibility. Since the element may not
 * be available upon instantiation of the component abstraction, this resolver lazily retrieves the element in question.
 */
@PublicInterface
public class WebElementResolver
{
	private final @NotNull Function<By, List<WebElement>> elementFinder;
	private final @NotNull By selector;

	/**
	 * Creates a new element resolver for a given selector.
	 *
	 * @param elementFinder
	 * 	The function to use to resolve the selector into {@code WebElement}S
	 * @param selector
	 * 	The selector that uniquely identifies the element in question
	 */
	@PublicInterface
	public WebElementResolver(Function<By, List<WebElement>> elementFinder, By selector)
	{
		this.elementFinder = elementFinder;
		this.selector = selector;
	}

	/**
	 * Returns the element, if present. If this method is called multiple times, the element is resolved anew for each
	 * invocation. This method requires that exactly one element be found using the selector. Otherwise an empty
	 * {@code Optional} will be returned.
	 */
	@PublicInterface
	public @NotNull Optional<WebElement> getElement()
	{
		WebElement retval = null;
		List<WebElement> elements = elementFinder.apply(selector);

		if (elements.size() == 1) {
			retval = elements.get(0);
		}

		return Optional.ofNullable(retval);
	}
}
