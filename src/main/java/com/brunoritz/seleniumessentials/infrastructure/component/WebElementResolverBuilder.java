/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.infrastructure.component;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import com.brunoritz.seleniumessentials.infrastructure.driver.WebtestDriverProvider;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

/**
 * Builds a web element resolver from a specified context (global or nested) and a variety of available selectors. See
 * the various {@code by} methods for details on the selectors.
 */
@PublicInterface
public class WebElementResolverBuilder
{
	private final @NotNull Function<By, List<WebElement>> elementFinder;

	/**
	 * Builds a resolver that looks for {@code WebElement}S on all of the visible page.
	 */
	@PublicInterface
	public static @NotNull WebElementResolverBuilder global()
	{
		return new WebElementResolverBuilder(
			selector -> WebtestDriverProvider.currentWebtestDriver().getElements(selector));
	}

	/**
	 * Builds a resolver that looks for {@code WebElement}S only within the subtree of an existing element.
	 */
	@PublicInterface
	public static @NotNull WebElementResolverBuilder nestedWithin(Component parent)
	{
		return new WebElementResolverBuilder(selector -> parent.getElement()
			.map(parentElement -> parentElement.findElements(selector))
			.orElse(Collections.emptyList()));
	}

	private WebElementResolverBuilder(Function<By, List<WebElement>> elementFinder)
	{
		this.elementFinder = elementFinder;
	}

	/**
	 * Creates an element supplier for elements by ID. The returned supplier matches any element having a {@code id}
	 * attribute with a value matching {@code id}.
	 */
	@PublicInterface
	public @NotNull WebElementResolver byId(String id)
	{
		return new WebElementResolver(elementFinder, By.id(id));
	}

	/**
	 * Creates an element supplier for elements by name. The returned supplier matches any element having a
	 * {@code name} attribute with a value matching {@code name}.
	 */
	@PublicInterface
	public @NotNull WebElementResolver byName(String name)
	{
		return new WebElementResolver(elementFinder, By.name(name));
	}

	/**
	 * Creates an element supplier for elements by {@code data-id}. The returned supplier matches any element having a
	 * {@code data-id} attribute with a value matching {@code id}.
	 */
	@PublicInterface
	public @NotNull WebElementResolver byDataId(String id)
	{
		return new WebElementResolver(elementFinder, By.cssSelector(String.format("*[data-id='%s']", id)));
	}

	/**
	 * Creates an element supplier for submit buttons. The returned supplier matches any element having a {@code type}
	 * attribute with a value of {@code submit}.
	 */
	@PublicInterface
	public @NotNull WebElementResolver submit()
	{
		return new WebElementResolver(elementFinder, By.cssSelector("*[type='submit']"));
	}

	/**
	 * Creates an element supplier for links. The returned supplier matches any link element whose text contents
	 * matches {@code text}.
	 * <p>
	 * Using this selector is not encouraged as the link text may eventually change, leading to unstable tests.
	 */
	@PublicInterface
	public @NotNull WebElementResolver byLinkText(String text)
	{
		return new WebElementResolver(elementFinder, By.linkText(text));
	}

	/**
	 * Creates an element supplier for specific tags nested within another element. The returned supplier matches any
	 * element whose tag name is equal to {@code tagName}.
	 */
	@PublicInterface
	public @NotNull WebElementResolver byTag(String tagName)
	{
		return new WebElementResolver(elementFinder, By.tagName(tagName));
	}

	/**
	 * Creates an element resolver for elements by CSS classes. The returned resolver matches any element having
	 * {@code className} as a CSS class. The element may have additional classes without impact to this resolver.
	 */
	@PublicInterface
	public @NotNull WebElementResolver byClass(String className)
	{
		return new WebElementResolver(elementFinder, By.className(className));
	}

	/**
	 * Creates an element resolver for elements by CSS selector. The returned resolver matches any element that can be
	 * found by the given CSS selector.
	 */
	@PublicInterface
	public @NotNull WebElementResolver byCssSelector(String cssSelector)
	{
		return new WebElementResolver(elementFinder, By.cssSelector(cssSelector));
	}
}
