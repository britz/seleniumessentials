/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.infrastructure.component;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebElement;

import java.util.Optional;

/**
 * The basic features any component has to support. Many features inside <i>Selenium Essentials</i> are based around the
 * concept of components.
 * <p>
 * A component may be something as small as a single HTML control or more complex like an entire form.
 */
@PublicInterface
public interface Component
{
	/**
	 * Indicates if this component is currently visible. If the HTML element cannot be resolved at the time of calling,
	 * this method will return {@code false}.
	 */
	@PublicInterface
	boolean isVisible();

	/**
	 * Returns the {@code WebElement} associated with this component. That element can be used for interacting with the
	 * element in the browser. It is recommended to use this method for any interaction. Note that on asynchronous
	 * pages the element does not always exist and probably has to be waited for before interacting with it.
	 *
	 * @return The browser element or  {@code Optional.empty()} if the element does not exist at the time of calling
	 */
	@PublicInterface
	@NotNull Optional<WebElement> getElement();
}
