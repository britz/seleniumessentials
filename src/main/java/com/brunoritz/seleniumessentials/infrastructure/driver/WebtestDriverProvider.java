/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.infrastructure.driver;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Provides webtests with driver instances.
 * <p>
 * This provider allows the driver for a thread be replaced with another one. This is helpful for integrating
 * <i>Selenium Essentials</i> into an existing test infrastructure as well as running tests in different browsers.
 * <p>
 * Note that in order for webtests to work with the default driver (Chrome), the {@code webdriver.chrome.driver} system
 * property needs to contain the fully qualified path to the Chrome Driver executable.
 */
@PublicInterface
public class WebtestDriverProvider
{
	private static final Lock lock = new ReentrantLock();
	private static final ThreadLocal<WebtestDriver> webtestDrivers =
		ThreadLocal.withInitial(WebtestDriverProvider::createWebtestDriver);
	private static final Map<Thread, WebDriver> seleniumDrivers = new HashMap<>(4);

	static {
		Runtime.getRuntime().addShutdownHook(new Thread(WebtestDriverProvider::cleanupDrivers));
	}

	private WebtestDriverProvider()
	{
		throw new UnsupportedOperationException("Use the static methods");
	}

	/**
	 * Returns the driver interface to be used for the current thread. The returned driver shall not be shared amongst
	 * multiple threads at the same time.
	 */
	@PublicInterface
	public static @NotNull WebtestDriver currentWebtestDriver()
	{
		lock.lock();

		try {
			return webtestDrivers.get();
		} finally {
			lock.unlock();
		}
	}

	private static @NotNull WebtestDriver createWebtestDriver()
	{
		WebDriver seleniumDriver =
			seleniumDrivers.computeIfAbsent(Thread.currentThread(), thread -> ChromeDriverFactory.newDriver());

		return new WebtestDriver(seleniumDriver);
	}

	/**
	 * Replaces the currently installed driver if the given condition holds true. The condition function will be passed
	 * in the current driver intance for evaluation. If no driver exists at the time of calling, {@code null} will be
	 * passed to the condition function.
	 * <p>
	 * The existing driver will be quit during the replacement, which will also close its browser windows.
	 *
	 * @param condition
	 * 	The condition to be true for the replacement to take place
	 * @param newDriverSupplier
	 * 	The supplier that will be used to obtain a new driver instance if {@code condition} is true
	 */
	@PublicInterface
	public static void replaceDriverIf(Function<WebDriver, Boolean> condition, Supplier<WebDriver> newDriverSupplier)
	{
		lock.lock();

		try {
			WebDriver currentDriver = seleniumDrivers.get(Thread.currentThread());

			if (condition.apply(currentDriver)) {
				if (currentDriver != null) {
					currentDriver.quit();
				}

				seleniumDrivers.remove(Thread.currentThread());
				seleniumDrivers.put(Thread.currentThread(), newDriverSupplier.get());
				webtestDrivers.remove();
			}
		} finally {
			lock.unlock();
		}
	}

	private static void cleanupDrivers()
	{
		lock.lock();

		try {
			seleniumDrivers.values().forEach(WebDriver::quit);
		} finally {
			lock.unlock();
		}
	}
}
