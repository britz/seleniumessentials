package com.brunoritz.seleniumessentials.infrastructure.driver;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

/**
 * This factory produces drivers for the Firefox browser. The produced drivers come with reasonable default settings, in
 * particular with proper scaling settings to help ensure better DPI independent operation.
 */
@PublicInterface
public class FirefoxDriverFactory
{
	private FirefoxDriverFactory()
	{
		throw new UnsupportedOperationException("Use static method");
	}

	@PublicInterface
	public static @NotNull WebDriver newDriver()
	{
		FirefoxBinary binary = new FirefoxBinary();
		FirefoxProfile profile = new FirefoxProfile();
		FirefoxOptions options = new FirefoxOptions();

		binary.addCommandLineOptions("-private");
		binary.addCommandLineOptions("-headless");

		profile.setPreference("intl.accept_languages", "en-us");
		profile.setPreference("layout.css.devPixelsPerPx", "1");

		options.setBinary(binary);
		options.setProfile(profile);

		return new FirefoxDriver(options);
	}
}
