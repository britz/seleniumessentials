/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.infrastructure.driver;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.function.Predicate;

/**
 * The interface to Selenium. All element access and other interaction with the browser and its content is made through
 * this class. This class acts as a wrapper around the low level Selenium API. Tests should never directly use the
 * Selenium API, but always use this class whenever possible.
 */
@PublicInterface
public class WebtestDriver
{
	private static final long WAIT_TIMEOUT_SECONDS = 20L;

	private final @NotNull WebDriver seleniumDriver;

	WebtestDriver(WebDriver seleniumDriver)
	{
		this.seleniumDriver = seleniumDriver;
		this.seleniumDriver.manage().window().setSize(new Dimension(1024, 768));
	}

	/**
	 * Returns all {@code WebElement}S identified by the specified selector.
	 *
	 * @param selector
	 * 	The selector for the elements to find
	 *
	 * @return The requested elements or empty if no element was found
	 */
	@PublicInterface
	public @NotNull List<WebElement> getElements(By selector)
	{
		return seleniumDriver.findElements(selector);
	}

	/**
	 * Await the given condition to be fulfilled. This method waits for a maximum of {@value #WAIT_TIMEOUT_SECONDS}
	 * seconds.
	 *
	 * @param condition
	 * 	The condition to test
	 *
	 * @throws TimeoutException
	 * 	If the condition did not become true within {@value #WAIT_TIMEOUT_SECONDS} seconds
	 */
	public void await(Predicate<WebDriver> condition)
	{
		Wait<WebDriver> wait = new WebDriverWait(seleniumDriver, WAIT_TIMEOUT_SECONDS);

		wait.until(condition::test);
	}

	/**
	 * Executes JavaScript code on the currently visible page and returns the result prduced by the script.
	 *
	 * @param scriptCode
	 * 	The JavaScript code to execute
	 * @param arguments
	 * 	The arguments to pass to the script code. The code can access them via the {@code arguments} array
	 *
	 * @return The result value of the script
	 */
	@PublicInterface
	public Object executeScript(String scriptCode, Object... arguments)
	{
		return ((JavascriptExecutor) seleniumDriver).executeScript(scriptCode, arguments);
	}

	/**
	 * Creates an {@code Actions} instance that allows for fine grained interaction with page elements.
	 */
	@PublicInterface
	public @NotNull Actions createActions()
	{
		return new Actions(seleniumDriver);
	}

	/**
	 * Loads the page with the given URL and waits for the page to be loaded. Once the page has been loaded, control is
	 * returned to the caller. This method does not wait for any JavaScript on the page be finished.
	 */
	@PublicInterface
	public void loadPage(String url)
	{
		seleniumDriver.get(url);
	}

	/**
	 * Resizes the test browser's viewport to the given size.
	 * <p>
	 * Note that this method may not succeed if the requested size is bigger than the available screen. In order to
	 * maximize success, this method moves the browser to point (0, 0) before resizing it so that it always can consume
	 * the full width and height of the screen.
	 */
	public void resizeViewport(Dimension newSize)
	{
		Long totalWidth =
			(Long) executeScript("return Math.max(window.outerWidth - window.innerWidth, 0) + arguments[0];",
				newSize.getWidth()
			);
		Long totalHeight =
			(Long) executeScript("return Math.max(window.outerHeight - window.innerHeight, 0) + arguments[0];",
				newSize.getHeight()
			);
		Dimension calculatedDimension = new Dimension(totalWidth.intValue(), totalHeight.intValue());
		Window browserWindow = seleniumDriver.manage().window();

		browserWindow.setPosition(new Point(0, 0));
		browserWindow.setSize(calculatedDimension);
	}

	/**
	 * Captures a screenshot of the browser's viewport.
	 *
	 * @throws IOException
	 * 	If the screenshot could not be taken
	 */
	@PublicInterface
	public @NotNull BufferedImage getScreenshot()
		throws IOException
	{
		TakesScreenshot screenshotter = (TakesScreenshot) seleniumDriver;
		byte[] screenshotBytes = screenshotter.getScreenshotAs(OutputType.BYTES);

		return ImageIO.read(new ByteArrayInputStream(screenshotBytes));
	}
}
