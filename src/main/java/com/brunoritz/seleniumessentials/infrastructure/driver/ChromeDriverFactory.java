package com.brunoritz.seleniumessentials.infrastructure.driver;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * This factory produces drivers for the Chrome browser. The produced drivers come with reasonable default settings, in
 * particular with proper scaling settings to help ensure better DPI independent operation.
 */
@PublicInterface
public class ChromeDriverFactory
{
	private ChromeDriverFactory()
	{
		throw new UnsupportedOperationException("Use static method");
	}

	@PublicInterface
	public static @NotNull WebDriver newDriver()
	{
		ChromeOptions chromeOptions = new ChromeOptions();

		// Incognito mode cannot be used here due to a defect
		// https://bugs.chromium.org/p/chromedriver/issues/detail?id=513
		chromeOptions.addArguments("--force-device-scale-factor=1");
		chromeOptions.addArguments("--mute-audio");
		chromeOptions.addArguments("--no-experiments");

		chromeOptions.setHeadless(true);

		return new ChromeDriver(chromeOptions);
	}
}
