/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.infrastructure.conditional;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import com.brunoritz.seleniumessentials.infrastructure.driver.WebtestDriverProvider;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.openqa.selenium.TimeoutException;

import java.util.function.Supplier;

/**
 * This asserter provides two different types of assertions:
 * <ul>
 * <li>immediate assertions</li>
 * <li>eventual assertions</li>
 * </ul>
 * <p>
 * An <i>immediate</i> assertion has to be true at the time of invocation. An <i>eventual</i> assertion has to become
 * true within a specific amount of time.
 * <p>
 * Eventual assertions are intented to be used whenever changes in the user interface are expected to happen as a
 * result of an asynchronous operation (AJAX call, client side JavaScript etc.).
 * <p>
 * Using {@code WebtestAsserter} allows to write more understandable assertions that reveal to a reader whether they
 * are expected to hold true immediately or eventually.
 * <pre>
 *     assertThat(immediately(() -> header.isVisible()));
 *     assertThat(eventually(() -> !loadingSpinner.isVisible()));
 * </pre>
 */
@PublicInterface
public class WebtestAsserter
{
	private WebtestAsserter()
	{
		throw new UnsupportedOperationException("Use assertThat()");
	}

	/**
	 * Assert that a given condition is holding true.
	 *
	 * @param assertion
	 * 	The assertion to test
	 */
	@PublicInterface
	public static void assertThat(WebtestAssertion assertion)
	{
		Assert.assertTrue(assertion.isFulfilled());
	}

	/**
	 * Requires that the given condition be fulfilled at the time of invocation. The condition is evaluated once.
	 *
	 * @param condition
	 * 	The condition to evaluate
	 *
	 * @return An assertion that can be used with {@link #assertThat(WebtestAssertion)}
	 */
	@PublicInterface
	public static @NotNull WebtestAssertion immediately(Supplier<Boolean> condition)
	{
		return condition::get;
	}

	/**
	 * Requires that the given condition be fulfilled in the near future. The condition is evaluated periodically
	 * until it either is fulfilled or a timeout occurs.
	 *
	 * @param condition
	 * 	The condition to evaluate
	 *
	 * @return An assertion that can be used with {@link #assertThat(WebtestAssertion)}
	 */
	@PublicInterface
	public static @NotNull WebtestAssertion eventually(Supplier<Boolean> condition)
	{
		return () -> {
			try {
				WebtestDriverProvider.currentWebtestDriver().await(webDriver -> condition.get());
				return true;
			} catch (TimeoutException ignored) {
				return false;
			}
		};
	}
}
