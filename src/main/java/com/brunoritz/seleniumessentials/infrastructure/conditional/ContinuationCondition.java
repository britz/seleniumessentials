/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.infrastructure.conditional;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import com.brunoritz.seleniumessentials.infrastructure.driver.WebtestDriverProvider;
import org.openqa.selenium.TimeoutException;

import java.util.function.Supplier;

/**
 * Continuation conditionas are used to enforce technical conditions before proceeding to the next step. They are
 * primarily used in page objects and components. They should never be used to await business related conditions. To
 * wait for business related conditions, assertions should be used.
 * <p>
 * The purpose of the continuation condition is to allow developers to separate between business related conditions
 * and technical conditions.
 */
@PublicInterface
public class ContinuationCondition
{
	private ContinuationCondition()
	{
		throw new UnsupportedOperationException("Use continueOnce()");
	}

	/**
	 * Interrupts the webtest code execution until the given condition has been satisfied. The condition is evaluated
	 * periodically until it either is fulfilled or a timeout occurs.
	 *
	 * @param condition
	 * 	The condition to evaluate
	 *
	 * @throws TimeoutException
	 * 	If the condition failed to become true
	 */
	@PublicInterface
	public static void continueOnce(Supplier<Boolean> condition)
	{
		WebtestDriverProvider.currentWebtestDriver().await(webDriver -> condition.get());
	}
}
