/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.component;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import com.brunoritz.seleniumessentials.infrastructure.component.Component;
import com.brunoritz.seleniumessentials.infrastructure.component.WebElementResolver;
import org.hamcrest.Matcher;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Objects;
import java.util.Optional;

@PublicInterface
public class TableRow
	implements Component
{
	private final @NotNull WebElementResolver resolver;

	TableRow(WebElementResolver resolver)
	{
		this.resolver = resolver;
	}

	@Override
	public boolean isVisible()
	{
		return getElement()
			.map(WebElement::isDisplayed)
			.orElse(false);
	}

	@Override
	public @NotNull Optional<WebElement> getElement()
	{
		return resolver.getElement();
	}

	@PublicInterface
	public boolean isColumnMatching(int columnIndex, Matcher<String> matcher)
	{
		return getColumnByIndex(columnIndex)
			.map(column -> matcher.matches(column.getText()))
			.orElse(false);
	}

	@PublicInterface
	public boolean isColumnEmpty(int columnIndex)
	{
		return getColumnByIndex(columnIndex)
			.map(column -> Objects.equals("", column.getText()) && column.findElements(By.cssSelector("*")).isEmpty())
			.orElse(true);
	}

	private @NotNull Optional<WebElement> getColumnByIndex(int columnIndex)
	{
		By columnSelector = By.cssSelector(String.format("td:nth-child(%d)", columnIndex + 1));

		return getElement()
			.map(self -> self.findElements(columnSelector))
			.filter(elements -> elements.size() == 1)
			.map(elements -> elements.get(0));
	}
}
