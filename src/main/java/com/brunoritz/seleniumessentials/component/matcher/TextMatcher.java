/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.component.matcher;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import org.hamcrest.Matcher;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * The matcher for text content in components. Matchers allow for authoring readable text matching without forcing
 * component authors to provide various methods (plain text, regex etc) for text matching.
 * <p>
 * A text matching assertion could simply look like this:
 * <pre>
 *     assertThat(eventually(() -> myComponent.isTextMatching(regex("\\d+")));
 *     ...
 *     assertThat(eventually(() -> myComponent.isTextMatching(literalText("Hello world")));
 * </pre>
 */
@PublicInterface
public enum TextMatcher
{
	;

	/**
	 * Returns a matcher for exact text matches. The matcher only returns {@code true} if the actual and expected texts
	 * match exactly (taking case sensitivity into consideration).
	 *
	 * @param expected
	 * 	The expected text (may be null)
	 */
	@PublicInterface
	public static @NotNull Matcher<String> literalText(@Nullable String expected)
	{
		return new PlainTextMatcher(expected);
	}

	/**
	 * Returns a matcher that attempts to match a text agains a given regular expression. The matcher will return
	 * {@code true} if the given input text can be matched by the regular expression in {@code expression}.
	 *
	 * @param expression
	 * 	The regular expression to match texts agianst
	 *
	 * @throws PatternSyntaxException
	 * 	If the expression's syntax is invalid
	 */
	@PublicInterface
	public static @NotNull Matcher<String> regex(String expression)
	{
		Pattern pattern = Pattern.compile(expression);

		return new RegexMatcher(pattern);
	}
}
