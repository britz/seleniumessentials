/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.browser;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import org.jetbrains.annotations.NotNull;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Adds support for running a single test or entire test class in a different browser. When this rule is present, a
 * test method or class can be annotated with {@link TargetBrowser} to define the target browser. If both, the test
 * class and the method are annotated, the method annotation will take precedence.
 * <p>
 * Note that switching browsers may take some time and slow down tests. It is therefore recommended to make use of this
 * only where really needed.
 * <p>
 * If rules are chained using a {@code RuleChain}, it is important that this rule be executed prior to any other rule
 * accessing the {@code WebtestDriver}. Otherwise the other rules may operate on a wrong browser instance.
 * <p>
 * If a test method or class specifies a browser that cannot be started, all tests supposed to run in that browser will
 * fail.
 *
 * @see TargetBrowser
 */
@PublicInterface
public class TargetBrowserRule
	implements TestRule
{
	private final @NotNull BrowserType defaultBrowser;

	/**
	 * Creates a new browser switching rule that uses the specified default browser if no annotations are defined.
	 * <p>
	 * The returned instance will determine the target browser using the following algorithm:
	 * <ol>
	 * <li>
	 * If the test method is annotated with {@code TargetBrowser}, use the browser specified in that annotation
	 * </li>
	 * <li>
	 * If the test method has no {@code TargetBrowser} annotation, but one is present on the test class, use the
	 * browser
	 * browser specified in that annotation
	 * </li>
	 * <li>Otherwise, use the browser passed via this method's {@code defaultBrowser} argument</li>
	 * </ol>
	 */
	@PublicInterface
	public static @NotNull TestRule withDefaultBrowser(BrowserType defaultBrowser)
	{
		return new TargetBrowserRule(defaultBrowser);
	}

	private TargetBrowserRule(@NotNull BrowserType defaultBrowser)
	{
		this.defaultBrowser = defaultBrowser;
	}

	@Override
	public @NotNull Statement apply(Statement base, Description description)
	{
		BrowserType targetBrowser = getTargetBrowser(description);

		return new BrowserSwitcher(base, targetBrowser);
	}

	private @NotNull BrowserType getTargetBrowser(Description description)
	{
		BrowserType retval;
		TargetBrowser classTargetBrowser = description.getTestClass().getAnnotation(TargetBrowser.class);
		TargetBrowser methodTargetBrowser = description.getAnnotation(TargetBrowser.class);

		if (methodTargetBrowser != null) {
			retval = methodTargetBrowser.value();
		} else if (classTargetBrowser != null) {
			retval = classTargetBrowser.value();
		} else {
			retval = defaultBrowser;
		}

		return retval;
	}
}
