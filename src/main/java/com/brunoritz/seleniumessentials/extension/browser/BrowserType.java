/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.browser;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import com.brunoritz.seleniumessentials.infrastructure.driver.ChromeDriverFactory;
import com.brunoritz.seleniumessentials.infrastructure.driver.FirefoxDriverFactory;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.util.function.Supplier;

/**
 * The browser types in which webtests can be executed. For any types to work it is required to have the corresponding
 * browser and its driver installed.
 */
@PublicInterface
public enum BrowserType
{
	/**
	 * Causes a test to be run in Google Chrome. Using this browser requires that the {@code webdriver.chrome.driver}
	 * system property be set to contain the fully qualified path to the Chrome Driver executable.
	 */
	CHROME(ChromeDriver.class, ChromeDriverFactory::newDriver),

	/**
	 * Causes a test to be run in Mozilla Firefox. Using this browser requires that the {@code webdriver.gecko.driver}
	 * system property be set to contain the fully qualified path to the Chrome Driver executable.
	 */
	FIREFOX(FirefoxDriver.class, FirefoxDriverFactory::newDriver),

	/**
	 * Causes a test to be run in Apple Safari. This option will only work on macOS with Safari 10.
	 */
	SAFARI(SafariDriver.class, SafariDriver::new),

	/**
	 * Causes a test to be run in Microsoft Edge. This option will only work on Windows 10.
	 */
	EDGE(EdgeDriver.class, EdgeDriver::new);

	private final @NotNull Class<? extends WebDriver> driverClass;
	private final @NotNull Supplier<WebDriver> driverFactory;

	BrowserType(Class<? extends WebDriver> driverClass, Supplier<WebDriver> driverFactory)
	{
		this.driverClass = driverClass;
		this.driverFactory = driverFactory;
	}

	boolean isDifferentType(WebDriver currentDriver)
	{
		return !driverClass.isInstance(currentDriver);
	}

	@NotNull WebDriver newDriver()
	{
		return driverFactory.get();
	}
}
