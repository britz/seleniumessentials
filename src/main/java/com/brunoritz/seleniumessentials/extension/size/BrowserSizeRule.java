/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.size;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import com.brunoritz.seleniumessentials.infrastructure.driver.WebtestDriverProvider;
import org.jetbrains.annotations.NotNull;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.openqa.selenium.Dimension;

import java.util.Optional;

/**
 * Adds support for resizing the browser window of a Selenium webtest. When this rule is present, a test method can be
 * annotated with {@link BrowserSize} to set the desired viewport size.
 * <p>
 * This is a helpful rule for testing responsive behavior.
 *
 * @see BrowserSize
 */
@PublicInterface
public class BrowserSizeRule
	implements TestRule
{
	private static final int DESKTOP_WIDTH = 1280;
	private static final int DESKTOP_HEIGHT = 1024;

	private final @NotNull Dimension defaultSize;

	/**
	 * Creates a new browser resize rule. The new rule will resize the browser to a desktop size
	 * ({@value #DESKTOP_WIDTH} by {@value #DESKTOP_HEIGHT}), whenever no {@code BrowserSize} annotation is
	 * present on a test.
	 */
	@PublicInterface
	public static @NotNull TestRule defaultToDesktopSize()
	{
		return new BrowserSizeRule(new Dimension(DESKTOP_WIDTH, DESKTOP_HEIGHT));
	}

	private BrowserSizeRule(@NotNull Dimension defaultSize)
	{
		this.defaultSize = defaultSize;
	}

	@Override
	public @NotNull Statement apply(@NotNull Statement base, @NotNull Description description)
	{
		Dimension desiredSize = getAnnotatedSize(description).orElse(defaultSize);

		return new BrowserResizer(base, desiredSize, WebtestDriverProvider::currentWebtestDriver);
	}

	private static @NotNull Optional<Dimension> getAnnotatedSize(Description description)
	{
		Dimension retval = null;
		BrowserSize browserSize = description.getAnnotation(BrowserSize.class);

		if (browserSize != null) {
			retval = new Dimension(browserSize.width(), browserSize.height());
		}

		return Optional.ofNullable(retval);
	}
}
