/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.regression.rule;

import com.brunoritz.seleniumessentials.extension.regression.assertion.BaselineImageResolver;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * Resolves images stored in the classpath. If the image exists, this resolver will create a temporary file holding a
 * copy of the requested file's contents.
 */
class ClasspathBasedBaselineImageResolver
	implements BaselineImageResolver
{
	private static final Pattern PACKAGE_SEPARATOR = Pattern.compile("\\.");

	private final @NotNull String packageName;

	ClasspathBasedBaselineImageResolver(String packageName)
	{
		this.packageName = packageName;
	}

	@Override
	public @NotNull Optional<Path> resolve(String filename)
	{
		Optional<Path> retval = Optional.empty();
		String resourceName = toFullyQualifiedName(filename);

		try (InputStream input = getClass().getResourceAsStream(resourceName)) {
			if (input != null) {
				Path tempFile = Files.createTempFile("baseline", ".png");

				FileUtils.copyInputStreamToFile(input, tempFile.toFile());

				retval = Optional.of(tempFile);
			}
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}

		return retval;
	}

	private @NotNull String toFullyQualifiedName(String filename)
	{
		String baseName = PACKAGE_SEPARATOR.matcher(packageName).replaceAll("/");

		return String.format("/%s/%s", baseName, filename);
	}
}
