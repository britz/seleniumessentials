/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.regression.rule;

import com.brunoritz.seleniumessentials.extension.regression.assertion.BaselineImageResolver;
import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Specifies where/how the baseline images are stored. Basline images can either be stored on the classpath or in an
 * arbitrary directory.
 * <p>
 * Storing the baseline images in the classpath is preferred, since the classpath is accessible on any platform, while
 * directories are platform and machine dependent. Using a modern build system (and IDE), the baseline screenshots can
 * be placed in the test resources. This allows them to be accessed via the classpath and therefore be portable.
 */
@PublicInterface
public enum BaselineStorage
{
	/**
	 * Indicates that the visual baselines are stored in the classpath of the test JVM. This is the recommended type to
	 * use since this is portable.
	 */
	CLASSPATH {
		@Override
		public @NotNull BaselineImageResolver createResolver(String storageLocation)
		{
			return new ClasspathBasedBaselineImageResolver(storageLocation);
		}
	},

	/**
	 * Indicates that the visual baselines are stored in a directory. This type should only be used with caution as it
	 * is not portable.
	 */
	DIRECTORY {
		@Override
		public @NotNull BaselineImageResolver createResolver(String storageLocation)
		{
			Path storagePath = Paths.get(storageLocation);

			return new DirectoryBasedBaselineImageResolver(storagePath);
		}
	};

	/**
	 * Creates a baseline image resolver that resolves images stored in the specified location. The format of
	 * {@code storageLocation} depends on the chosen storage type.
	 */
	public abstract @NotNull BaselineImageResolver createResolver(String storageLocation);
}
