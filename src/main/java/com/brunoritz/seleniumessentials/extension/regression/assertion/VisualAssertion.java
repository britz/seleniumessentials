/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.regression.assertion;

import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import com.brunoritz.seleniumessentials.infrastructure.component.Component;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebElement;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Visual assertions allow comparing screenshots of a page or parts thereof against baseline images. This allows a test
 * to cover not only text content of a page, but also to ensure that the visual appearance remains correct over time.
 * Changes to visual appearance can happen by changes to stylesheets or structural changes. Many of these changes
 * would not be detected by simply evaluating text content of the page.
 * <p>
 * Doing such visual checks by evaluating an element's CSS values, dimensions etc would result in very complex tests
 * that would require complex maintenance. Working with image comparison and highlighting of differences makes this
 * task more efficient by allowing a user to review visual changes and decide if they are correct or indicate an
 * error. If changes are expected, they can be included in the baseline images.
 * <p>
 * The comparison of the images allows changes to happen within some tolerance so that differences caused by font
 * rendering (anti-aliasing may produce different results based on the screen where the text is displayed) do not
 * result in a failing test.
 * <p>
 * If differences are detected that are outside the tolerated limits, this rule will save the current screenshot and
 * an image with highlighted differences to the differences directory for review.
 */
@PublicInterface
public class VisualAssertion
{
	private static final @NotNull Logger logger = Logger.getLogger(VisualAssertion.class.getName());

	private final @NotNull BaselineImageResolver baselineImageResolver;
	private final @NotNull Path differenceDirectory;
	private final @NotNull ScreenshotComposer composer;

	public VisualAssertion(BaselineImageResolver baselineImageResolver, Path differenceDirectory,
		ScreenshotComposer composer)
	{
		this.baselineImageResolver = baselineImageResolver;
		this.differenceDirectory = differenceDirectory;
		this.composer = composer;
	}

	/**
	 * Removes a part of the visible area from the visual comparison. It is recommended to exclude areas that have
	 * content which may change frequently (videos, live feeds, news roller, clocks etc.).
	 * <p>
	 * By calling this method, the respective area on the screen is replaced with a black rectangle.
	 *
	 * @param excluded
	 * 	The component whose area to exclude from the visual comparison
	 *
	 * @throws IllegalArgumentException
	 * 	If {@code excluded} is outside the screenshot boundaries
	 */
	@PublicInterface
	public @NotNull VisualAssertion excluding(Component excluded)
	{
		WebElement excludedElement = excluded.getElement()
			.orElseThrow(() -> new IllegalStateException("Element does not exist"));

		composer.hideArea(excludedElement);

		return this;
	}

	/**
	 * Compares the current view of the page against an expected baseline screenshot. This method allows for some
	 * tolerance without failing the comparison. This is particularly relevant for rendered fonts as their appearance
	 * may vary depending on the screen and antialiasing method.
	 *
	 * @param baselineScreenId
	 * 	The name of the baseline screen to compare the current screenshot against
	 *
	 * @return A boolean indicating whether the screenshot is matching the baseline
	 *
	 * @throws MissingBaselineException
	 * 	If the basline screenshot does not exist
	 * @throws IllegalStateException
	 * 	If the image comparison cannot be executed
	 */
	@PublicInterface
	public boolean isMatchingBaseline(String baselineScreenId)
	{
		try {
			return isMatchingBaselineWithTolerance(baselineScreenId);
		} catch (IOException | InterruptedException e) {
			logger.log(Level.SEVERE, "Failed to copmpare images", e);

			throw new IllegalStateException("Failed to compare images", e);
		}
	}

	private boolean isMatchingBaselineWithTolerance(String baselineScreenId)
		throws IOException, InterruptedException
	{
		Path currentScreenshot = saveScreenshotForComparison(baselineScreenId);
		Path baselineScreenshot = getBaselinePath(baselineScreenId);
		Path diffs = createDifferenceHighlightFile(baselineScreenId);
		boolean matching = ImageComparison.newComparison()
			.withBaselineImage(baselineScreenshot)
			.withCurrentScreenshot(currentScreenshot)
			.withDiffTarget(diffs)
			.withTolerance(0.5f)
			.isMatching();

		if (!matching) {
			String details = String.format("Current screenshot (%s) differs (%s) from baseline '%s'",
				currentScreenshot, diffs, baselineScreenId
			);

			logger.info(details);
		}

		return matching;
	}

	private @NotNull Path getBaselinePath(String baselineScreenId)
	{
		String baselineScreenshotFilename = String.format("%s.png", baselineScreenId);

		return baselineImageResolver.resolve(baselineScreenshotFilename)
			.orElseThrow(() -> new MissingBaselineException(baselineScreenId));
	}

	private @NotNull Path createDifferenceHighlightFile(String baselineScreenId)
		throws IOException
	{
		return Files.createTempFile(differenceDirectory, baselineScreenId, "-diff.png");
	}

	private @NotNull Path saveScreenshotForComparison(String baselineScreenId)
		throws IOException
	{
		Path currentScreenshot = Files.createTempFile(differenceDirectory, baselineScreenId, "-current.png");
		BufferedImage screenshot = composer.compose();

		ImageIO.write(screenshot, "png", currentScreenshot.toFile());

		return currentScreenshot;
	}
}
