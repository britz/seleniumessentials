/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.regression.assertion;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Compares two images for equality. This implementation allows for some differences between the images, so that font
 * rendering differences do not lead to failures.
 * <p>
 * If the two images are considered different, all differences exceeding the tolerance will be highlighted in a third
 * image for later analysis.
 * <p>
 * This implementation uses <i>ImageMagick</i> for comparison.
 */
class ImageComparison
{
	private final @NotNull Path compareApplication;

	private @Nullable Path baselineImage;
	private @Nullable Path currentScreenshot;
	private @Nullable Path diffTarget;
	private float tolerance;

	/**
	 * Sets up a new image comparison. The returned instance has to be configured using the {@code with} methods
	 * prior to invoking {@link #isMatching()}.
	 * <p>
	 * The instance returned by this method can be configured in a chained method call. Each {@code with} method will
	 * return the same instance as it was called on.
	 */
	static @NotNull ImageComparison newComparison()
	{
		Path comparePath = locateCompare();

		return new ImageComparison(comparePath);
	}

	private ImageComparison(Path compareApplication)
	{
		this.compareApplication = compareApplication;
	}

	/**
	 * Specifies the baseline image to use in the comparison. The baseline image is the image expected to be visible in
	 * the browser's viewport.
	 */
	@NotNull ImageComparison withBaselineImage(Path value)
	{
		baselineImage = value;

		return this;
	}

	/**
	 * Specifies the current screenshot to use in the comparison. This is the image as rendered by the browser.
	 */
	@NotNull ImageComparison withCurrentScreenshot(Path value)
	{
		currentScreenshot = value;

		return this;
	}

	/**
	 * Specifies the image that should receive the annotated/highlighted differences. This image will only be produced
	 * if the baseline image and the current screenshot have the same size.
	 */
	@NotNull ImageComparison withDiffTarget(Path value)
	{
		diffTarget = value;

		return this;
	}

	/**
	 * Specifies the tolerance for allowed difference in color. This has to be a value from in the range from 0.0f to
	 * 1.0f (inclusive).
	 * For details on its meaning, see https://www.imagemagick.org/script/command-line-options.php#fuzz
	 *
	 * @throws IllegalArgumentException
	 * 	If {@code value} is outside the acceptable range
	 */
	@NotNull ImageComparison withTolerance(float value)
	{
		if ((value < 0.0f) || (value > 1.0f)) {
			throw new IllegalArgumentException("Tolerance outside acceptable range");
		}

		tolerance = value;

		return this;
	}

	/**
	 * Indicates if the images are matching each other. This method allows for some tolerance without considering the
	 * images different. Internally, this method delegates the comparison to ImageMagick. The {@code compare} utility
	 * will be called with the following options, see ImageMagick documentation for details on their meaning.
	 * <ul>
	 * <li>-metric AE</li>
	 * <li>-fuzz &lt;tolerance&gt;%</li>
	 * </ul>
	 *
	 * @throws IOException
	 * 	If the comparison failed
	 * @throws InterruptedException
	 * 	If the comparison failed
	 */
	boolean isMatching()
		throws IOException, InterruptedException
	{
		ProcessBuilder compareCommand = createCompareCall();
		Process compareProcess = compareCommand.start();
		int exitCode = compareProcess.waitFor();

		return (exitCode == 0);
	}

	private @NotNull ProcessBuilder createCompareCall()
	{
		if ((baselineImage == null) || (currentScreenshot == null) || (diffTarget == null)) {
			throw new IllegalStateException("Instance not fully configured");
		}

		return new ProcessBuilder(compareApplication.toString(),
			"-metric", "AE",
			"-fuzz", String.format("%f%%", tolerance * 100.0f),
			baselineImage.toString(),
			currentScreenshot.toString(),
			diffTarget.toString()
		)
			.directory(null)
			.inheritIO();
	}

	private static @NotNull Path locateCompare()
	{
		String[] environmentDirectories = System.getenv("PATH").split(File.pathSeparator);

		return Arrays.stream(environmentDirectories)
			.map(Paths::get)
			.flatMap(ImageComparison::compareCandidates)
			.filter(path -> Files.exists(path))
			.findFirst()
			.orElseThrow(() -> new IllegalStateException("compare not found in PATH"));
	}

	private static @NotNull Stream<Path> compareCandidates(Path directory)
	{
		Path unixName = directory.resolve("compare");
		Path windowsName = directory.resolve("compare.exe");

		return Stream.of(unixName, windowsName);
	}
}
