/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.regression.assertion;

import com.brunoritz.seleniumessentials.infrastructure.driver.WebtestDriver;
import com.brunoritz.seleniumessentials.infrastructure.driver.WebtestDriverProvider;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.UncheckedIOException;

/**
 * Composes an image for comparison against a baseline. Each screenshot starts from a screenshot of the container
 * element. If needed, areas can be excluded from the screenshot (@see {@link #hideArea(WebElement)}). Excluded areas
 * are replaced with a black area.
 */
public class ScreenshotComposer
{
	private final @NotNull Rectangle screenshotBounds;
	private final @NotNull BufferedImage screenshotImage;
	private final @NotNull Graphics2D paintArea;

	/**
	 * Starts composing a screenshot. {@code container} defines what is included in the screenshot. All elements
	 * contained in {@code container} are included in the screenshot.
	 * <p>
	 * It is suggested to call this method only when the page is in a ready to compare state (no more content loading).
	 * Otherwise the resulting screenshot may be inconsistent.
	 *
	 * @param container
	 * 	The area to screenshot
	 *
	 * @throws UncheckedIOException
	 * 	If the initial screenshot chould not be taken
	 */
	public static @NotNull ScreenshotComposer newScreenshot(WebElement container)
	{
		WebtestDriver webtestDriver = WebtestDriverProvider.currentWebtestDriver();

		return new ScreenshotComposer(webtestDriver, container);
	}

	ScreenshotComposer(WebtestDriver webtestDriver, WebElement container)
	{
		try {
			screenshotImage = webtestDriver.getScreenshot();
			screenshotBounds = getBoundingRect(container);
			paintArea = screenshotImage.createGraphics();
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	/**
	 * Blacks out the region covered by an element from the screenshot. It is suggested to black out all regions that
	 * may change frequently and have no relevance in a visual regression test (such as a box containing a live feed).
	 *
	 * @param elementToHide
	 * 	The element representing the area to black out
	 *
	 * @throws IllegalArgumentException
	 * 	If {@code areaToHide} is outside the screenshot boundaries
	 */
	void hideArea(WebElement elementToHide)
	{
		Rectangle areaToHide = getBoundingRect(elementToHide);

		ensureAreaOverlapsVisibleArea(areaToHide);
		blackoutArea(areaToHide);
	}

	private void ensureAreaOverlapsVisibleArea(Rectangle areaToHide)
	{
		int rightmostVisible = screenshotBounds.getX() + screenshotBounds.getWidth();
		int bottommostVisible = screenshotBounds.getY() + screenshotBounds.getHeight();
		int rightmostHiding = areaToHide.getX() + areaToHide.getWidth();
		int bottommostHiding = areaToHide.getY() + areaToHide.getHeight();

		if ((areaToHide.getX() > rightmostVisible)
			|| (areaToHide.getY() > bottommostVisible)
			|| (rightmostHiding < screenshotBounds.getX())
			|| (bottommostHiding < screenshotBounds.getY())) {
			throw new IllegalArgumentException("Area is outside visible screenshot");
		}
	}

	private void blackoutArea(Rectangle areaToHide)
	{
		paintArea.setColor(Color.BLACK);
		paintArea.fillRect(areaToHide.getX(), areaToHide.getY(), areaToHide.getWidth(), areaToHide.getHeight());
	}

	/**
	 * Returns the final screenshots with all excluded areas blacked out.
	 */
	@NotNull BufferedImage compose()
	{
		return screenshotImage.getSubimage(screenshotBounds.getX(), screenshotBounds.getY(),
			screenshotBounds.getWidth(), screenshotBounds.getHeight()
		);
	}

	private static @NotNull Rectangle getBoundingRect(WebElement element)
	{
		Point location = element.getLocation();
		Dimension size = element.getSize();

		return new Rectangle(location.getX(), location.getY(), size.height, size.getWidth());
	}
}
