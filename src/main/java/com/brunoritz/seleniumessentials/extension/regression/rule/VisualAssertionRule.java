/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.regression.rule;

import com.brunoritz.seleniumessentials.extension.regression.assertion.BaselineImageResolver;
import com.brunoritz.seleniumessentials.extension.regression.assertion.ScreenshotComposer;
import com.brunoritz.seleniumessentials.extension.regression.assertion.VisualAssertion;
import com.brunoritz.seleniumessentials.infrastructure.PublicInterface;
import com.brunoritz.seleniumessentials.infrastructure.component.Component;
import org.jetbrains.annotations.NotNull;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.annotation.Annotation;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.function.Function;

/**
 * A class rule to be applied to webtest classes that intend to perform visual regression tests.
 * <p>
 * This class rule allows the test methods to obtain instances of {@link VisualAssertion}S that are proeprly configured
 * for the test class being executed. A typical test would look about this:
 * <pre>
 *     &#064;VisualBaselines(...)
 *     public class SomeWebtest
 *     {
 *          &#064;ClassRule public static final VisualAssertionRule visualAsserter = new VisualAssertionRule();
 *
 *           &#064;Test
 *           public void testMethodWithVisualComparison()
 *           {
 *               Component header = new TextDisplay(global().byDataId("header));
 *               Component headerClock = new TextDisplay(nestedWithin(header).byDataId("clock"));
 *
 *               WebtestDriver.current().loadPage("http://www.page-under-test.com");
 *
 *               assertThat(eventually(() -> header.isVisible()));
 *               assertThat(immediately(() -> visualRule.visualAppearanceOf(header)
 *                   .excluding(headerClock)
 *                   .isMatchingReference("referenceScreenHeader")
 *                ));
 *           }
 *     }
 * </pre>
 * <p>
 * Each test class is required to have the metadata annotation {@link VisualBaselines}.
 * <p>
 * This rule has to be added as a class rule and instances cannot be shared amongst multiple classes.
 * <p>
 * For having reliable visual assertions, it is important to always run the test with the same browser dimensions. See
 * {@link com.brunoritz.seleniumessentials.extension.size.BrowserSize} for details.
 * <p>
 * For this rule to work properly, it is required that <i>ImageMagick</i> be installed and its binary location be
 * included in the {@code PATH} environment variable.
 */
public class VisualAssertionRule
	extends TestWatcher
{
	private static final @NotNull ThreadLocal<Description> currentTest = new ThreadLocal<>();

	private final @NotNull Function<WebElement, ScreenshotComposer> screenshotComposerBuilder;
	private final @NotNull Function<Class<? extends Annotation>, Optional<Annotation>> annotationFinder;

	@PublicInterface
	public VisualAssertionRule()
	{
		this(ScreenshotComposer::newScreenshot,
			annotationClass -> Optional.ofNullable(currentTest.get().getTestClass().getAnnotation(annotationClass)));
	}

	VisualAssertionRule(Function<WebElement, ScreenshotComposer> screenshotComposerBuilder,
		Function<Class<? extends Annotation>, Optional<Annotation>> annotationFinder)
	{
		this.screenshotComposerBuilder = screenshotComposerBuilder;
		this.annotationFinder = annotationFinder;
	}

	/**
	 * Creates a new visual assertion that can assert for the visual correctness of the area covered by the given
	 * component.
	 *
	 * @param screenshotScope
	 * 	The area to include in the visual comparison
	 *
	 * @throws IllegalStateException
	 * 	If the baseline resolver or the image difference directory have not been setup
	 */
	@PublicInterface
	public @NotNull VisualAssertion visualAppearanceOf(Component screenshotScope)
	{
		Description runningTest = currentTest.get();

		if (runningTest != null) {
			WebElement screenshotElement = screenshotScope.getElement()
				.orElseThrow(() -> new IllegalStateException("Element does not exist"));
			BaselineImageResolver baselineImageResolver = getVisualBaselineResolver();
			Path diffDirectory = createVisualDiffsDirectory(runningTest);
			ScreenshotComposer composer = screenshotComposerBuilder.apply(screenshotElement);

			return new VisualAssertion(baselineImageResolver, diffDirectory, composer);
		}

		throw new IllegalStateException("Missing baseline resolver or image difference directory");
	}

	@Override
	protected void starting(Description description)
	{
		currentTest.set(description);
	}

	@Override
	protected void finished(Description description)
	{
		currentTest.remove();
	}

	private @NotNull BaselineImageResolver getVisualBaselineResolver()
	{
		VisualBaselines baselineAnnotation = (VisualBaselines) annotationFinder.apply(VisualBaselines.class)
			.orElseThrow(() -> new IllegalStateException("Test class is missing VisualBaselines annotation"));

		return baselineAnnotation.storage().createResolver(baselineAnnotation.value());
	}

	private static @NotNull Path createVisualDiffsDirectory(Description description)
	{
		try {
			String directoryPrefix = String.format("visual-assertion-%s", description.getTestClass().getName());

			return Files.createTempDirectory(directoryPrefix);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}
