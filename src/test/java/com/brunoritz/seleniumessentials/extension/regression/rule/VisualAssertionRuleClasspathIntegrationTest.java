/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.regression.rule;

import com.brunoritz.seleniumessentials.component.TextDisplay;
import com.brunoritz.seleniumessentials.extension.LoadPage;
import com.brunoritz.seleniumessentials.extension.LoadPageRule;
import com.brunoritz.seleniumessentials.extension.regression.assertion.MissingBaselineException;
import com.brunoritz.seleniumessentials.infrastructure.component.Component;
import org.jetbrains.annotations.NotNull;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static com.brunoritz.seleniumessentials.infrastructure.component.WebElementResolverBuilder.global;
import static com.brunoritz.seleniumessentials.infrastructure.conditional.WebtestAsserter.assertThat;
import static com.brunoritz.seleniumessentials.infrastructure.conditional.WebtestAsserter.immediately;

@VisualBaselines(
	storage = BaselineStorage.CLASSPATH,
	value = "com.brunoritz.seleniumessentials.extension.regression.rule"
)
public class VisualAssertionRuleClasspathIntegrationTest
{
	@ClassRule
	public static final @NotNull VisualAssertionRule testee = new VisualAssertionRule();

	@Rule
	public final @NotNull TestRule loadPageRule = new LoadPageRule();

	@Test
	@LoadPage("VisualAssertionRuleTest-pageMatching.html")
	public void matchesReferenceScreenshot_viewMatchingReference_true()
	{
		Component regionOfInterest = new TextDisplay(global().byTag("div"));

		assertThat(immediately(() -> testee.visualAppearanceOf(regionOfInterest)
			.isMatchingBaseline("VisualAssertionRuleTest-pageMatching")
		));
	}

	@Test
	@LoadPage("VisualAssertionRuleTest-pageMismatching.html")
	public void matchesReferenceScreenshot_viewNotMatchingReference_false()
	{
		Component regionOfInterest = new TextDisplay(global().byTag("div"));

		assertThat(immediately(() -> !testee.visualAppearanceOf(regionOfInterest)
			.isMatchingBaseline("VisualAssertionRuleTest-pageMismatching")
		));
	}

	@Test(expected = MissingBaselineException.class)
	@LoadPage("VisualAssertionRuleTest-noBaseline.html")
	public void matchesReferenceScreenshot_noBaselineImage_exception()
	{
		Component regionOfInterest = new TextDisplay(global().byTag("div"));

		testee.visualAppearanceOf(regionOfInterest)
			.isMatchingBaseline("VisualAssertionRuleTest-noBaseline");
	}
}
