/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.regression.rule;

import com.brunoritz.seleniumessentials.extension.regression.assertion.BaselineImageResolver;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class DirectoryBasedBaselineImageResolverTest
{
	@Test
	public void resolve_existsInFilesystem_accessiblePathReturned()
		throws URISyntaxException
	{
		BaselineImageResolver testee = new DirectoryBasedBaselineImageResolver(getTestRestResourcePath());
		Path resolved = testee.resolve("locator.txt")
			.orElseThrow(() -> new IllegalStateException("Expected non-empty"));

		assertThat(Files.exists(resolved), equalTo(true));
		assertThat(Files.isRegularFile(resolved), equalTo(true));
	}

	@Test
	public void resolve_absentInFilesystem_empty()
		throws URISyntaxException
	{
		BaselineImageResolver testee = new DirectoryBasedBaselineImageResolver(getTestRestResourcePath());
		Optional<Path> resolved = testee.resolve("absent-file");

		assertThat(resolved.isPresent(), equalTo(false));
	}

	private @NotNull Path getTestRestResourcePath()
		throws URISyntaxException
	{
		URI locatorPath = getClass().getResource("locator.txt").toURI();

		return Paths.get(locatorPath).getParent();
	}
}
