/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.regression.rule;

import com.brunoritz.seleniumessentials.extension.regression.assertion.ScreenshotComposer;
import com.brunoritz.seleniumessentials.infrastructure.component.Component;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.Description;
import org.openqa.selenium.WebElement;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.annotation.Annotation;
import java.util.Optional;
import java.util.function.Function;

public class VisualAssertionRuleTest
{
	@Test(expected = IllegalStateException.class)
	public void visualAppearanceOf_missingBaselineAnnotation_exception()
	{
		Component componentMock = mockPresentComponent();
		Description testDescription = Description.createTestDescription(MissingBaselines.class, "");
		VisualAssertionRule testee = createVisualAssertionRule();

		testee.starting(testDescription);

		testee.visualAppearanceOf(componentMock);
	}

	@Test(expected = IllegalStateException.class)
	public void visualAppearanceOf_noTestRunning_exception()
	{
		Component componentMock = mockPresentComponent();
		VisualAssertionRule testee = createVisualAssertionRule();

		testee.visualAppearanceOf(componentMock);
	}

	private static @NotNull VisualAssertionRule createVisualAssertionRule()
	{
		Function<Class<? extends Annotation>, Optional<Annotation>> annotationFinder =
			anntationClass -> Optional.empty();

		return new VisualAssertionRule(webElement -> mock(ScreenshotComposer.class), annotationFinder);
	}

	private static @NotNull Component mockPresentComponent()
	{
		Component retval = mock(Component.class);
		WebElement elementMock = mock(WebElement.class);

		when(retval.getElement()).thenReturn(Optional.of(elementMock));

		return retval;
	}

	private static class MissingBaselines
	{
	}
}
