/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.regression.rule;

import com.brunoritz.seleniumessentials.component.TextDisplay;
import com.brunoritz.seleniumessentials.extension.LoadPage;
import com.brunoritz.seleniumessentials.extension.LoadPageRule;
import com.brunoritz.seleniumessentials.extension.regression.assertion.MissingBaselineException;
import com.brunoritz.seleniumessentials.extension.regression.assertion.ScreenshotComposer;
import com.brunoritz.seleniumessentials.infrastructure.component.Component;
import org.jetbrains.annotations.NotNull;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.Description;

import static com.brunoritz.seleniumessentials.infrastructure.component.WebElementResolverBuilder.global;
import static com.brunoritz.seleniumessentials.infrastructure.conditional.WebtestAsserter.assertThat;
import static com.brunoritz.seleniumessentials.infrastructure.conditional.WebtestAsserter.immediately;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.annotation.Annotation;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.function.Function;

public class VisualAssertionRuleDirectoryIntegrationTest
{
	@Rule
	public final @NotNull TestRule loadPageRule = new LoadPageRule();

	@Test
	@LoadPage("VisualAssertionRuleTest-pageMatching.html")
	public void matchesReferenceScreenshot_viewMatchingReference_true()
		throws URISyntaxException
	{
		Description testDescription = Description
			.createTestDescription(VisualAssertionRuleDirectoryIntegrationTest.class, "");
		Component regionOfInterest = new TextDisplay(global().byTag("div"));
		VisualAssertionRule testee = createDirectoryBasedRule();

		testee.starting(testDescription);

		assertThat(immediately(() -> testee.visualAppearanceOf(regionOfInterest)
			.isMatchingBaseline("VisualAssertionRuleTest-pageMatching")
		));
	}

	@Test
	@LoadPage("VisualAssertionRuleTest-pageMismatching.html")
	public void matchesReferenceScreenshot_viewNotMatchingReference_false()
		throws URISyntaxException
	{
		Description testDescription = Description
			.createTestDescription(VisualAssertionRuleDirectoryIntegrationTest.class, "");
		Component regionOfInterest = new TextDisplay(global().byTag("div"));
		VisualAssertionRule testee = createDirectoryBasedRule();

		testee.starting(testDescription);

		assertThat(immediately(() -> !testee.visualAppearanceOf(regionOfInterest)
			.isMatchingBaseline("VisualAssertionRuleTest-pageMismatching")
		));
	}

	@Test(expected = MissingBaselineException.class)
	@LoadPage("VisualAssertionRuleTest-noBaseline.html")
	public void matchesReferenceScreenshot_noBaselineImage_exception()
		throws URISyntaxException
	{
		Description testDescription = Description
			.createTestDescription(VisualAssertionRuleDirectoryIntegrationTest.class, "");
		Component regionOfInterest = new TextDisplay(global().byTag("div"));
		VisualAssertionRule testee = createDirectoryBasedRule();

		testee.starting(testDescription);

		testee.visualAppearanceOf(regionOfInterest)
			.isMatchingBaseline("VisualAssertionRuleTest-noBaseline");
	}

	private static @NotNull VisualAssertionRule createDirectoryBasedRule()
		throws URISyntaxException
	{
		VisualBaselines baselineAnnotationMock = mock(VisualBaselines.class);
		Function<Class<? extends Annotation>, Optional<Annotation>> annotationFinder =
			anntationClass -> Optional.of(baselineAnnotationMock);
		VisualAssertionRule retval = new VisualAssertionRule(ScreenshotComposer::newScreenshot, annotationFinder);

		when(baselineAnnotationMock.storage()).thenReturn(BaselineStorage.DIRECTORY);
		when(baselineAnnotationMock.value()).thenReturn(getResourcePath());

		return retval;
	}

	private static @NotNull String getResourcePath()
		throws URISyntaxException
	{
		URI locatorPath = VisualAssertionRuleDirectoryIntegrationTest.class.getResource("locator.txt").toURI();
		Path baselineDirectory = Paths.get(locatorPath).getParent();

		return baselineDirectory.toString();
	}
}
