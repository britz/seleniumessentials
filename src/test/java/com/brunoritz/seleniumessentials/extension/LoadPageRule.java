/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension;

import com.brunoritz.seleniumessentials.infrastructure.driver.WebtestDriverProvider;
import org.jetbrains.annotations.NotNull;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.net.URL;

public class LoadPageRule
	implements TestRule
{
	@Override
	public @NotNull Statement apply(@NotNull Statement base, @NotNull Description description)
	{
		LoadPage pageSpec = description.getAnnotation(LoadPage.class);
		Statement retval;

		if (pageSpec != null) {
			retval = new LoadPageStatement(description.getTestClass(), base, pageSpec.value());
		} else {
			retval = base;
		}

		return retval;
	}

	private static class LoadPageStatement
		extends Statement
	{
		private final @NotNull Class<?> testClass;
		private final @NotNull Statement wrapped;
		private final @NotNull String pageName;

		private LoadPageStatement(Class<?> testClass, Statement wrapped, String pageName)
		{
			this.testClass = testClass;
			this.wrapped = wrapped;
			this.pageName = pageName;
		}

		@Override
		public void evaluate()
			throws Throwable
		{
			loadPage();

			wrapped.evaluate();
		}

		private void loadPage()
		{
			URL resource = testClass.getResource(pageName);
			String path = String.format("file://%s", resource.getFile());

			WebtestDriverProvider.currentWebtestDriver().loadPage(path);
		}
	}
}
