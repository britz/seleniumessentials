/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.size;

import com.brunoritz.seleniumessentials.infrastructure.driver.WebtestDriver;
import com.brunoritz.seleniumessentials.infrastructure.driver.WebtestDriverProvider;
import org.jetbrains.annotations.NotNull;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class BrowserSizeRuleTest
{
	@Rule
	public final @NotNull TestRule testee = BrowserSizeRule.defaultToDesktopSize();

	@Test
	public void apply_noDimensionAnnotated_testRunAtDesktopDimension()
	{
		WebtestDriver driver = WebtestDriverProvider.currentWebtestDriver();
		Long innerWidth = (Long) driver.executeScript("return window.innerWidth");
		Long innerHeight = (Long) driver.executeScript("return window.innerHeight");

		assertThat(innerWidth, equalTo(1280L));
		assertThat(innerHeight, equalTo(1024L));
	}

	@Test
	@BrowserSize(width = 1024, height = 768)
	public void apply_dimensionAnnotated_testRunAtAnnotatedDimension()
	{
		WebtestDriver driver = WebtestDriverProvider.currentWebtestDriver();
		Long innerWidth = (Long) driver.executeScript("return window.innerWidth");
		Long innerHeight = (Long) driver.executeScript("return window.innerHeight");

		assertThat(innerWidth, equalTo(1024L));
		assertThat(innerHeight, equalTo(768L));
	}
}
