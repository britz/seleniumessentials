/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
 * LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.infrastructure.component

import com.brunoritz.seleniumessentials.LoadPage
import spock.lang.Specification

import static com.brunoritz.seleniumessentials.infrastructure.component.WebElementResolverBuilder.global

class WebElementResolverBuilderSpec
	extends Specification
{
	@LoadPage("WebElementResolverBuilderTest.html")
	"[Global Scope] Existing element shall be resolved"()
	{
		given:
			def resolver = global().byId("global_elementExists_resolved")

		when:
			def result = resolver.getElement()

		then:
			result.isPresent()
	}

	@LoadPage("WebElementResolverBuilderTest.html")
	"[Global scope] Absent element shall not be resolved"()
	{
		given:
			def resolver = global().byId("global_elementDoesNotExist_empty")

		when:
			def result = resolver.getElement()

		then:
			!result.isPresent()
	}

	@LoadPage("WebElementResolverBuilderTest.html")
	"[Global scope] Multiple matching elements shall not be resolved"()
	{
		given:
			def resolver = global().byTag("div")

		when:
			def result = resolver.getElement()

		then:
			!result.isPresent()
	}

	@LoadPage("WebElementResolverBuilderTest.html")
	"[Nested scope] Selectors shall be resolved relative to the parent element"()
	{
		given:
			def container = new ContainerStub(global().byId("nestedWithin_selectorEvaluatedInsideParent"))
			def nestedResolver = WebElementResolverBuilder.nestedWithin(container)
				.byDataId("matching")

		when:
			def nestedElement = nestedResolver.getElement()
			def elementText = nestedElement
				.map { element -> element.getText() }
				.orElse("Unexpected")

		then:
			elementText == "Expected"
	}

	@LoadPage("WebElementResolverBuilderTest.html")
	"[By ID] Element shall be resolved by matching its ID"()
	{
		given:
			def resolver = global().byId("byId_elementIsMatchedById")

		when:
			def resolvedElement = resolver.getElement()

		then:
			def elementText = resolvedElement
				.map { element -> element.getText() }
				.orElse("Unexpected")

			elementText == "byId_elementIsMatchedById"
	}

	@LoadPage("WebElementResolverBuilderTest.html")
	"[By Name] Element shall be resolved by matching its name"()
	{
		given:
			def resolver = global().byName("byName_elementIsMatchedByName")

		when:
			def resolvedElement = resolver.getElement()

		then:
			def elementText = resolvedElement
				.map { element -> element.getAttribute("value") }
				.orElse("Unexpected")

			elementText == "byName_elementIsMatchedByName"
	}

	@LoadPage("WebElementResolverBuilderTest.html")
	"[By Data ID] Element shall be resolved by matching its data-id"()
	{
		given:
			def resolver = global().byDataId("byDataId_elementIsMatchedByDataId")

		when:
			def resolvedElement = resolver.getElement()

		then:
			def elementText = resolvedElement
				.map { element -> element.getText() }
				.orElse("Unexpected")

			elementText == "byDataId_elementIsMatchedByDataId"
	}

	@LoadPage("WebElementResolverBuilderTest.html")
	"[Submit Button] Element shall be resolved by matching its submit type"()
	{
		given:
			def resolver = global().submit()

		when:
			def resolvedElement = resolver.getElement()

		then:
			def elementText = resolvedElement
				.map { element -> element.getText() }
				.orElse("Unexpected")

			elementText == "submit_elementIsMatchedBySubmitType"
	}

	@LoadPage("WebElementResolverBuilderTest.html")
	"[By Link Text] Element shall be resolved by matching its link text"()
	{
		given:
			def resolver = global().byLinkText("byLinkText_elementIsMatchedPartialLinkText")

		when:
			def resolvedElement = resolver.getElement()

		then:
			def elementText = resolvedElement
				.map { element -> element.getText() }
				.orElse("Unexpected")

			elementText == "byLinkText_elementIsMatchedPartialLinkText"
	}

	@LoadPage("WebElementResolverBuilderTest.html")
	"[By Tag] Element shall be resolved by matching its HTML tag name"()
	{
		given:
			def resolver = global().byTag("span")

		when:
			def resolvedElement = resolver.getElement()

		then:
			def elementText = resolvedElement
				.map { element -> element.getText() }
				.orElse("Unexpected")

			elementText == "byTag_elementIsMatchedByHtmlTag"
	}

	@LoadPage("WebElementResolverBuilderTest.html")
	"[By CSS Class] Element shall be resolved by matching the presence of a specified CSS class"()
	{
		given:
			def resolver = global().byClass("expected-class")

		when:
			def resolvedElement = resolver.getElement()

		then:
			def elementText = resolvedElement
				.map { element -> element.getText() }
				.orElse("Unexpected")

			elementText == "byClass_elementIsMatchedByCssClass"
	}

	@LoadPage("WebElementResolverBuilderTest.html")
	"[By CSS Selector] Element shall be resolved by evaluating the given CSS selector expression"()
	{
		given:
			def resolver = global().byCssSelector("*[data-id='byCssSelector_elementIsMatchedCssExpression']")

		when:
			def resolvedElement = resolver.getElement()

		then:
			def elementText = resolvedElement
				.map { element -> element.getText() }
				.orElse("Unexpected")

			elementText == "byCssSelector_elementIsMatchedCssExpression"
	}
}
