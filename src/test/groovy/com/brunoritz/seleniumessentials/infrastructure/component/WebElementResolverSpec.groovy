/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
 * LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.infrastructure.component

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import spock.lang.Specification

class WebElementResolverSpec
	extends Specification
{
	def "Resolver shall return an empty Optional if no matching element is found"()
	{
		given:
			def elementFinder = { by -> [] }
			def testee = new WebElementResolver(elementFinder, By.cssSelector("dummy"))

		when:
			def result = testee.getElement()

		then:
			!result.isPresent()
	}

	def "Resolver shall return the matching element if exactly one instance is found"()
	{
		given:
			def elementFinder = { by -> [Mock(WebElement)] }
			def testee = new WebElementResolver(elementFinder, By.cssSelector("dummy"))

		when:
			def result = testee.getElement()

		then:
			result.isPresent()
	}

	def "Resolver shall return an empty Optional if multiple matching elements are found"()
	{
		given:
			def elementFinder = { by -> [Mock(WebElement), Mock(WebElement)] }
			def testee = new WebElementResolver(elementFinder, By.cssSelector("dummy"))

		when:
			def result = testee.getElement()

		then:
			!result.isPresent()
	}

	def "Resolving the element shall use the specified selector"()
	{
		given:
			def expectedSelector = By.cssSelector("dummy")
			def elementMock = Mock(WebElement)
			def elementFinder = { selector ->
				if (expectedSelector == selector) {
					return Collections.singletonList(elementMock)
				} else {
					return Collections.emptyList()
				}
			}
			def testee = new WebElementResolver(elementFinder, expectedSelector)

		when:
			def result = testee.getElement()

		then:
			result.isPresent()
	}
}
