package com.brunoritz.seleniumessentials.infrastructure.component

import org.jetbrains.annotations.NotNull
import org.openqa.selenium.WebElement

class ContainerStub
	implements Component
{
	WebElementResolver resolver

	ContainerStub(WebElementResolver resolver)
	{
		this.resolver = resolver
	}

	boolean isVisible()
	{
		return false
	}

	@NotNull Optional<WebElement> getElement()
	{
		return resolver.getElement()
	}
}
