package com.brunoritz.seleniumessentials.infrastructure.conditional

import java.util.function.Supplier

class StaticBooleanSupplier
	implements Supplier<Boolean>
{
	boolean value

	StaticBooleanSupplier(boolean value)
	{
		this.value = value
	}

	Boolean get()
	{
		return value
	}
}
