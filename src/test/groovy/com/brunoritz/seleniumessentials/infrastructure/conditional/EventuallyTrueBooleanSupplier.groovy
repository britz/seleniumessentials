package com.brunoritz.seleniumessentials.infrastructure.conditional

import java.util.function.Supplier

class EventuallyTrueBooleanSupplier
	implements Supplier<Boolean>
{
	int count

	Boolean get()
	{
		count++

		return count == 4
	}
}
