/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
 * LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.infrastructure.conditional

import spock.lang.Specification

class WebtestAsserterSpec
	extends Specification
{
	def "If an assertion is not fulfilled, it shall throw an AssertionError"()
	{
		given:
			def condition = { -> false }

		when:
			WebtestAsserter.assertThat(condition)

		then:
			thrown(AssertionError)
	}

	def "If an immediate assertion is fulfilled, no expcetion should be thrown"()
	{
		given:
			def condition = Spy(StaticBooleanSupplier, constructorArgs: [true])

		when:
			WebtestAsserter.assertThat(WebtestAsserter.immediately(condition))

		then:
			1 * condition.get()
			notThrown(AssertionError)
	}

	def "If an immediate assertion is not fulfilled, an AssertionError shall be thrown"()
	{
		given:
			def condition = Spy(StaticBooleanSupplier, constructorArgs: [false])

		when:
			WebtestAsserter.assertThat(WebtestAsserter.immediately(condition))

		then:
			1 * condition.get()
			thrown(AssertionError)
	}

	def "If an eventual assertion is eventually fulfilled, no expcetion shall be thrown"()
	{
		given:
			def eventualCondition = Spy(EventuallyTrueBooleanSupplier)

		when:
			WebtestAsserter.assertThat(WebtestAsserter.eventually(eventualCondition))

		then:
			4 * eventualCondition.get()
			notThrown(AssertionError)
	}
}
