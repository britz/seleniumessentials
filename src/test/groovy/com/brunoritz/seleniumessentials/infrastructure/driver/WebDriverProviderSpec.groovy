/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
 * LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.infrastructure.driver

import spock.lang.Specification

import java.util.concurrent.ExecutorCompletionService
import java.util.concurrent.Executors

class WebDriverProviderSpec
	extends Specification
{
	def "The provider shall return the same driver instance for one thread if invoked multiple times"()
	{
		given:
			def firstInstance = WebtestDriverProvider.currentWebtestDriver()
			def secondInstance = WebtestDriverProvider.currentWebtestDriver()

		expect:
			firstInstance.is(secondInstance)
	}

	def "Each thread shall get an excluside driver instance"()
	{
		given:
			def firstInstance = getDriverFromNewThread()
			def secondInstance = getDriverFromNewThread()

		expect:
			!firstInstance.is(secondInstance)
	}

	def "Driver shall be replaced if replacement condition is fulfilled"()
	{
		given:
			def firstInstance = WebtestDriverProvider.currentWebtestDriver()

		when:
			WebtestDriverProvider.replaceDriverIf { driver -> true } { -> FirefoxDriverFactory.newDriver() }

		then:
			def newInstance = WebtestDriverProvider.currentWebtestDriver()

			!firstInstance.is(newInstance)
	}

	def "Driver instance shall remain unchanged if replacement condition is not fulfilled"()
	{
		given:
			def firstInstance = WebtestDriverProvider.currentWebtestDriver()

		when:
			WebtestDriverProvider.replaceDriverIf { driver -> false } { -> FirefoxDriverFactory.newDriver() }

		then:
			def secondInstance = WebtestDriverProvider.currentWebtestDriver()

			firstInstance.is(secondInstance)
	}

	def getDriverFromNewThread()
	{
		def pool = Executors.newFixedThreadPool(1)
		def service1 = new ExecutorCompletionService<>(pool)
		def driverJob = service1.submit { -> WebtestDriverProvider.currentWebtestDriver() }

		return driverJob.get()
	}
}
