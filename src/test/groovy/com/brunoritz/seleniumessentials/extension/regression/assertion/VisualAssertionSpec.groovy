/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
 * LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.regression.assertion

import com.brunoritz.seleniumessentials.LoadPage
import com.brunoritz.seleniumessentials.component.TextDisplay
import com.brunoritz.seleniumessentials.infrastructure.component.Component
import org.apache.commons.io.FileUtils
import spock.lang.Specification

import static com.brunoritz.seleniumessentials.infrastructure.component.WebElementResolverBuilder.global

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class VisualAssertionSpec
	extends Specification
{
	Path differenceDirectory

	def setup()
	{
		differenceDirectory = Files.createTempDirectory("VisualAssertionTest")
	}

	def cleanup()
	{
		if (differenceDirectory != null) {
			FileUtils.deleteQuietly(differenceDirectory.toFile())
		}
	}

	@LoadPage("VisualAssertionTest-pageMatching.html")
	"If the actual screenshot is matching the reference the assertion shall pass"()
	{
		given:
			def baselineResolver = getBaselineImageResolver()
			def screenshotComposer = getScreenshotComposer()
			def testee = new VisualAssertion(baselineResolver, differenceDirectory, screenshotComposer)

		when:
			def isMatching = testee.isMatchingBaseline("VisualAssertionTest-pageMatching")

		then:
			isMatching
			outputImageExists("VisualAssertionTest-pageMatching.*?-diff.png")
			outputImageExists("VisualAssertionTest-pageMatching.*?-current.png")
	}

	@LoadPage("VisualAssertionTest-pageMismatching.html")
	"If the actual screenshot is not the reference the assertion shall fail"()
	{
		given:
			def baselineResolver = getBaselineImageResolver()
			def screenshotComposer = getScreenshotComposer()
			def testee = new VisualAssertion(baselineResolver, differenceDirectory, screenshotComposer)

		when:
			def isMatching = testee.isMatchingBaseline("VisualAssertionTest-pageMismatching")

		then:
			!isMatching
			outputImageExists("VisualAssertionTest-pageMismatching.*?-diff.png")
			outputImageExists("VisualAssertionTest-pageMismatching.*?-current.png")
	}

	@LoadPage("VisualAssertionTest-ignoredDifference.html")
	"Ignored areas the are different shall not lead to failure"()
	{
		given:
			def baselineResolver = getBaselineImageResolver()
			def ignoredSection = new TextDisplay(global().byDataId("to-ignore"))
			def screenshotComposer = getScreenshotComposer()
			def testee = new VisualAssertion(baselineResolver, differenceDirectory, screenshotComposer)

		and:
			testee.excluding(ignoredSection)

		when:
			def isMatching = testee.isMatchingBaseline("VisualAssertionTest-ignoredDifference")

		then:
			isMatching
			outputImageExists("VisualAssertionTest-ignoredDifference.*?-diff.png")
			outputImageExists("VisualAssertionTest-ignoredDifference.*?-current.png")
	}

	@LoadPage("VisualAssertionTest-noBaseline.html")
	"The assertion shall fail if no baseline image can be found"()
	{
		given:
			def baselineResolver = { name -> Optional.empty() }
			def screenshotComposer = getScreenshotComposer()
			def testee = new VisualAssertion(baselineResolver, differenceDirectory, screenshotComposer)

		when:
			testee.isMatchingBaseline("VisualAssertionTest-missingBaseline")

		then:
			thrown(MissingBaselineException)
			outputImageExists("VisualAssertionTest-missingBaseline.*?-current.png")
	}

	BaselineImageResolver getBaselineImageResolver()
	{
		def locatorPath = getClass().getResource("locator.txt").toURI()
		def baselineDirectory = Paths.get(locatorPath).getParent()

		return { imageName -> Optional.of(baselineDirectory.resolve(imageName)) }
	}

	def getScreenshotComposer()
	{
		def wholePage = new TextDisplay(global().byTag("div"))

		return ScreenshotComposer.newScreenshot(element(wholePage))
	}

	def element(Component component)
	{
		return component.getElement()
			.orElseThrow { -> new IllegalStateException("Element does not exist") }
	}

	def outputImageExists(String filenameRegex)
	{
		def pathFilter = { path -> path.getFileName().toString().matches(filenameRegex) }

		return Files.newDirectoryStream(differenceDirectory, pathFilter)
			.withCloseable { paths -> paths.iterator().hasNext() }
	}
}
