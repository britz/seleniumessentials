/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.extension.regression.assertion

import com.brunoritz.seleniumessentials.infrastructure.driver.WebtestDriver
import org.openqa.selenium.Dimension
import org.openqa.selenium.Point
import org.openqa.selenium.WebElement
import spock.lang.Specification

import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import java.awt.image.RenderedImage

class ScreenshotComposerSpec
	extends Specification
{
	def "Full screenshot shall be returned if the target rectangle covers the screentho's bounds"()
	{
		given:
			def screenshot = loadImage("ScreenshotComposerTest-unaltered.png")
			def webtestDriverMock = driverThatProvidesScreenshot(screenshot)
			def screenshotTargetMock = elementWithBounds(0, 0, screenshot.getHeight(), screenshot.getWidth())
			def testee = new ScreenshotComposer(webtestDriverMock, screenshotTargetMock)

		when:
			def actualImage = testee.compose()

		then:
			areImagesEqual(screenshot, actualImage)
	}

	def "A subimage shall be returned if the target rectangle is smaller than the full screenshot"()
	{
		given:
			def screenshot = loadImage("ScreenshotComposerTest-subimage-input.png")
			def webtestDriverMock = driverThatProvidesScreenshot(screenshot)
			def screenshotTargetMock = elementWithBounds(37, 22, 105, 159)
			def testee = new ScreenshotComposer(webtestDriverMock, screenshotTargetMock)

		when:
			def actualImage = testee.compose()

		then:
			def expectedResult = loadImage("ScreenshotComposerTest-subimage-result.png")

			areImagesEqual(expectedResult, actualImage)
	}

	def "Unwanted areas shall be blacked out in the resulting image"()
	{
		given:
			def screenshot = loadImage("ScreenshotComposerTest-hiding-input.png")
			def webtestDriverMock = driverThatProvidesScreenshot(screenshot)
			def screenshotTargetMock = elementWithBounds(0, 0, screenshot.getHeight(), screenshot.getWidth())
			def testee = new ScreenshotComposer(webtestDriverMock, screenshotTargetMock)

			testee.hideArea(elementWithBounds(6, 9, 32, 36))
			testee.hideArea(elementWithBounds(112, 67, 32, 39))

		when:
			def actualImage = testee.compose()

		then:
			def expectedResult = loadImage("ScreenshotComposerTest-hiding-input-result.png")

			areImagesEqual(expectedResult, actualImage)
	}

	def "Hiding an area outside the screenshot (top left) boundaries shall not be allowed"()
	{
		given:
			def screenshot = loadImage("ScreenshotComposerTest-rectOutside.png")
			def webtestDriverMock = driverThatProvidesScreenshot(screenshot)
			def screenshotTargetMock = elementWithBounds(10, 10, screenshot.getHeight(), screenshot.getWidth())
			def testee = new ScreenshotComposer(webtestDriverMock, screenshotTargetMock)

		when:
			def elementOutsideBounds = elementWithBounds(0, 0, 5, 5)

			testee.hideArea(elementOutsideBounds)

		then:
			thrown(IllegalArgumentException)
	}

	def "Hiding an area outside the screenshot (bottom right) boundaries shall not be allowed"()
	{
		given:
			def screenshot = loadImage("ScreenshotComposerTest-rectOutside.png")
			def webtestDriverMock = driverThatProvidesScreenshot(screenshot)
			def screenshotTargetMock = elementWithBounds(0, 0, screenshot.getHeight(), screenshot.getWidth())
			def testee = new ScreenshotComposer(webtestDriverMock, screenshotTargetMock)

		when:
			def elementOutsideBounds = elementWithBounds(screenshot.getWidth() + 1, screenshot.getHeight() + 1, 1, 1)

			testee.hideArea(elementOutsideBounds)

		then:
			thrown(IllegalArgumentException)
	}

	def "Failure to capture a screenshot shall be propagated to the caller"()
	{
		given:
			def webtestDriverMock = Mock(WebtestDriver) {
				getScreenshot() >> {
					throw new IOException("Simulated failure")
				}
			}
			def webElementMock = Mock(WebElement)

		when:
			new ScreenshotComposer(webtestDriverMock, webElementMock)

		then:
			thrown(UncheckedIOException)
	}

	BufferedImage loadImage(String resourceName)
	{
		return getClass().getResourceAsStream(resourceName)
			.withStream { InputStream inputStream -> ImageIO.read(inputStream) }
	}

	def driverThatProvidesScreenshot(BufferedImage screenshot)
	{
		return Mock(WebtestDriver) {
			getScreenshot() >> screenshot
		}
	}

	def elementWithBounds(int x, int y, int height, int width)
	{
		return Mock(WebElement) {
			getLocation() >> new Point(x, y)
			getSize() >> new Dimension(width, height)
		}
	}

	def areImagesEqual(BufferedImage img1, BufferedImage img2)
	{
		return areDimensionsEqual(img1, img2) && areContentsEqual(img1, img2)
	}

	def areDimensionsEqual(RenderedImage img1, RenderedImage img2)
	{
		return (img1.getWidth() == img2.getWidth()) && (img1.getHeight() == img2.getHeight())
	}

	def areContentsEqual(BufferedImage img1, BufferedImage img2)
	{
		def retval = true

		for (def x = 0; x < img1.getWidth(); x++) {
			for (def y = 0; y < img1.getHeight(); y++) {
				def pixelsIdentical = img1.getRGB(x, y) == img2.getRGB(x, y)

				retval &= pixelsIdentical
			}
		}

		return retval
	}
}
