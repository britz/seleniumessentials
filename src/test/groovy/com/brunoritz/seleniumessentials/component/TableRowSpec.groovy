/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.component

import com.brunoritz.seleniumessentials.LoadPage
import spock.lang.Specification

import static com.brunoritz.seleniumessentials.component.matcher.TextMatcher.literalText
import static com.brunoritz.seleniumessentials.infrastructure.component.WebElementResolverBuilder.global

class TableRowSpec
	extends Specification
{
	@LoadPage("TableRowTest-tableWithBody.html")
	"A column with an expected text content shall be considered matching"()
	{
		given:
			def table = new Table(global().byTag("table"))
			def testee = table.getBodyRow(0)

		when:
			def textMatching = testee.isColumnMatching(0, literalText("Content"))

		then:
			textMatching
	}

	@LoadPage("TableRowTest-tableWithBody.html")
	"An inexistent column shall be considered a mismatch"()
	{
		given:
			def table = new Table(global().byTag("table"))
			def testee = table.getBodyRow(0)

		when:
			def textMatching = testee.isColumnMatching(99, literalText("Content"))

		then:
			!textMatching
	}

	@LoadPage("TableRowTest-tableWithBody.html")
	"An existing column with an unexpected text content shall be considered a mismatch"()
	{
		given:
			def table = new Table(global().byTag("table"))
			def testee = table.getBodyRow(0)

		when:
			def textMatching = testee.isColumnMatching(0, literalText("Not matching"))

		then:
			!textMatching
	}

	@LoadPage("TableRowTest-tableWithBody.html")
	"An inexistent column shall be considered empty"()
	{
		given:
			def table = new Table(global().byTag("table"))
			def testee = table.getBodyRow(0)

		when:
			def isEmpty = testee.isColumnEmpty(99)

		then:
			isEmpty
	}

	@LoadPage("TableRowTest-tableWithBody.html")
	"An existing columen with a text content shall not be considered empty"()
	{
		given:
			def table = new Table(global().byTag("table"))
			def testee = table.getBodyRow(0)

		when:
			def isEmpty = testee.isColumnEmpty(0)

		then:
			!isEmpty
	}

	@LoadPage("TableRowTest-tableWithBody.html")
	"An exsisting column with neither text content nor nested elements shall be considered empty"()
	{
		given:
			def table = new Table(global().byTag("table"))
			def testee = table.getBodyRow(1)

		when:
			def isEmpty = testee.isColumnEmpty(0)

		then:
			isEmpty
	}

	@LoadPage("TableRowTest-tableWithBody.html")
	"An existing column with a nested element shall not be considered empty"()
	{
		given:
			def table = new Table(global().byTag("table"))
			def testee = table.getBodyRow(1)

		when:
			def isEmpty = testee.isColumnEmpty(1)

		then:
			!isEmpty
	}
}
