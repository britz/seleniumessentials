/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.component

import com.brunoritz.seleniumessentials.LoadPage
import spock.lang.Specification

import static com.brunoritz.seleniumessentials.component.matcher.TextMatcher.literalText
import static com.brunoritz.seleniumessentials.infrastructure.component.WebElementResolverBuilder.global

class TextFieldSpec
	extends Specification
{
	@LoadPage("empty.html")
	"An absent text field shall be considered invisible"()
	{
		given:
			def testee = new TextField(global().byTag("input"))

		when:
			def isVisible = testee.isVisible()

		then:
			!isVisible
	}

	@LoadPage("TextFieldTest-inputInvisible.html")
	"An existing but hidden text field shall be considered invisible"()
	{
		given:
			def testee = new TextField(global().byTag("input"))

		when:
			def isVisible = testee.isVisible()

		then:
			!isVisible
	}

	@LoadPage("TextFieldTest-inputVisible.html")
	"An existing text field being displayed shall be considered visible"()
	{
		given:
			def testee = new TextField(global().byTag("input"))

		when:
			def isVisible = testee.isVisible()

		then:
			isVisible
	}

	@LoadPage("empty.html")
	"An absent text display shall not match any text"()
	{
		given:
			def testee = new TextField(global().byTag("input"))

		when:
			def isTextMatching = testee.isTextMatching(literalText("any"))

		then:
			!isTextMatching
	}

	@LoadPage("TextFieldTest-inputVisible.html")
	"A visible text field shall allow text matching"()
	{
		given:
			def testee = new TextField(global().byTag("input"))

		when:
			def isTextMatching = testee.isTextMatching(literalText("Visible text"))

		then:
			isTextMatching
	}

	@LoadPage("TextFieldTest-inputInvisible.html")
	"An invisible text field shall not allow setting text"()
	{
		given:
			def testee = new TextField(global().byTag("input"))

		when:
			testee.setText("Fail")

		then:
			thrown(IllegalStateException)
	}

	@LoadPage("TextFieldTest-inputDisabled.html")
	"A disabled text field shall not allow setting text"()
	{
		given:
			def testee = new TextField(global().byTag("input"))

		when:
			testee.setText("Fail")

		then:
			thrown(IllegalStateException)
	}

	@LoadPage("TextFieldTest-inputVisible.html")
	"An enabled and visible text field shall allow setting text"()
	{
		given:
			def testee = new TextField(global().byTag("input"))

		when:
			testee.setText("New Text")

		then:
			testee.isTextMatching(literalText("New Text"))
	}
}
