/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.component.matcher

import spock.lang.Specification

class TextMatcherSpec
	extends Specification
{
	def "[Literal text] Matching null against null shall be considered matching"()
	{
		given:
			def testee = TextMatcher.literalText(null)

		when:
			def isMatching = testee.matches(null)

		then:
			isMatching
	}

	def "[Literal text] Matching null against an expected text shall be considered a mismatch"()
	{
		given:
			def testee = TextMatcher.literalText("expected")

		when:
			def isMatching = testee.matches(null)

		then:
			!isMatching
	}

	def "[Literal text] Matching a text against a different one shall be considered a mismatch"()
	{
		given:
			def testee = TextMatcher.literalText("expected")

		when:
			def isMatching = testee.matches("actual")

		then:
			!isMatching
	}

	def "[Literal text] Matching a text against the exactly same text shall be considered a match"()
	{
		given:
			def testee = TextMatcher.literalText("same")

		when:
			def isMatching = testee.matches("same")

		then:
			isMatching
	}

	def "[Literal text] Matching a text against the same text in different casing shall be considered a mismatch"()
	{
		given:
			def testee = TextMatcher.literalText("same")

		when:
			def isMatching = testee.matches("SAM")

		then:
			!isMatching
	}

	def "[Regular expression] Invalid regex pattern shall not be accepted"()
	{
		when:
			TextMatcher.regex("(")

		then:
			thrown(IllegalArgumentException)
	}

	def "[Regular expression] Matching text against a mismatching pattern shall be considered a mismatch"()
	{
		given:
			def testee = TextMatcher.regex("\\d+")

		when:
			def isMatching = testee.matches("actual")

		then:
			!isMatching
	}

	def "[Regular expression] Matching text against a matching pattern shall be considered a match"()
	{
		given:
			def testee = TextMatcher.regex("\\d+")

		when:
			def isMatching = testee.matches("1234")

		then:
			isMatching
	}
}
