/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.component

import com.brunoritz.seleniumessentials.LoadPage
import com.brunoritz.seleniumessentials.infrastructure.component.WebElementResolver
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import spock.lang.Specification

import static com.brunoritz.seleniumessentials.component.matcher.TextMatcher.literalText
import static com.brunoritz.seleniumessentials.infrastructure.component.WebElementResolverBuilder.global

class ButtonSpec
	extends Specification
{
	@LoadPage("empty.html")
	"An absent button shall be considered invisible"()
	{
		given:
			def testee = new Button(global().byTag("button"))

		when:
			def isVisible = testee.isVisible()

		then:
			!isVisible
	}

	@LoadPage("ButtonTest-buttonInvisible.html")
	"An existing but hidden link shall be considered invisible"()
	{
		given:
			def testee = new Button(global().byTag("button"))

		when:
			def isVisible = testee.isVisible()

		then:
			!isVisible
	}

	@LoadPage("ButtonTest-buttonVisible.html")
	"An existing button being displayed shall be considered visible"()
	{
		given:
			def testee = new Button(global().byTag("button"))

		when:
			def isVisible = testee.isVisible()

		then:
			isVisible
	}

	@LoadPage("empty.html")
	"An absent button shall not allow invocation"()
	{
		given:
			def testee = new Button(global().byTag("button"))

		when:
			testee.invoke()

		then:
			thrown(IllegalStateException)
	}

	@LoadPage("ButtonTest-buttonDisabled.html")
	"A disabled button shall not allow invocation"()
	{
		given:
			def testee = new Button(global().byTag("button"))

		when:
			testee.invoke()

		then:
			thrown(IllegalStateException)
	}

	@LoadPage("empty.html")
	"An absent button shall not match any text"()
	{
		given:
			def testee = new Button(global().byTag("button"))

		when:
			def isTextMatching = testee.isTextMatching(literalText("any"))

		then:
			!isTextMatching
	}

	@LoadPage("ButtonTest-buttonVisible.html")
	"A visible button shall allow text matching"()
	{
		given:
			def testee = new Button(global().byTag("button"))

		when:
			def isTextMatching = testee.isTextMatching(literalText("Button Text"))

		then:
			isTextMatching
	}

	def "Invoking a button shall result in clicking on it"()
	{
		given:
			def buttonElementMock = Mock(WebElement) {
				isEnabled() >> true
				isVisible() >> true
			}
			def resolutionFunction = { elector -> Collections.singletonList(buttonElementMock) }
			def resolver = new WebElementResolver(resolutionFunction, By.cssSelector("*"))
			def testee = new Button(resolver)

		when:
			testee.invoke()

		then:
			1 * buttonElementMock.click()
	}
}
