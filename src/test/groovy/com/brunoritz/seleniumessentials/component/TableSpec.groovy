/*
 * Copyright (c) 2016-2017, Bruno Ritz <bruno.ritz@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Bruno Ritz nor the names of any contributor may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BRUNO RITZ BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.brunoritz.seleniumessentials.component

import com.brunoritz.seleniumessentials.LoadPage
import spock.lang.Specification

import static com.brunoritz.seleniumessentials.infrastructure.component.WebElementResolverBuilder.global

class TableSpec
	extends Specification
{
	@LoadPage("empty.html")
	"An absent table shall be considered invisible"()
	{
		given:
			def testee = new Table(global().byTag("table"))

		when:
			def isVisible = testee.isVisible()

		then:
			!isVisible
	}

	@LoadPage("TableTest-tableInvisible.html")
	"An existing but hidden table shall be considered invisible"()
	{
		given:
			def testee = new Table(global().byTag("table"))

		when:
			def isVisible = testee.isVisible()

		then:
			!isVisible
	}

	@LoadPage("TableTest-tableWithBody.html")
	"An existing table being displayed shall be considered visible"()
	{
		given:
			def testee = new Table(global().byTag("table"))

		when:
			def isVisible = testee.isVisible()

		then:
			isVisible
	}

	@LoadPage("empty.html")
	"An absent table shall not report the presence of any body rows"()
	{
		given:
			def testee = new Table(global().byTag("table"))

		when:
			def numberOfBodyRows = testee.getNumberOfBodyRows()

		then:
			numberOfBodyRows == 0
	}

	@LoadPage("TableTest-tableWithBody.html")
	"A visible table shall report the nubmer of rows in its TBODY tag"()
	{
		given:
			def testee = new Table(global().byTag("table"))

		when:
			def numberOfBodyRows = testee.getNumberOfBodyRows()

		then:
			numberOfBodyRows == 4
	}

	@LoadPage("TableTest-tableWithBody.html")
	"Existing rows shall be considered visible"()
	{
		given:
			def testee = new Table(global().byTag("table"))

		when:
			def row = testee.getBodyRow(1)

		then:
			row.isVisible()
	}

	@LoadPage("TableTest-tableWithBody.html")
	"Inexistent rows shall be considered invisible"()
	{
		given:
			def testee = new Table(global().byTag("table"))

		when:
			def row = testee.getBodyRow(999)

		then:
			!row.isVisible()
	}
}
