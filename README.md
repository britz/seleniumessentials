# Selenium Essentials

## About ##

### Description

_Selenium Essentials_ is a small library useful for writing readable web tests. It provides an easy to use API on top of
the standard Selenium API.

The syntax and the concepts of this library were inspired by the author's daily experience with web tests and their
shortcomings. This library does not claim to be "best practices" in any way as what really is "best" depended on
context. It is rather a "my personal successful practices" sort of thing.

_Selenium Essentials_ contains an experimental image comparison for implementing visual regression tests where current
screenshots are compared against baslines to find unwanted changes.

### License

_Selenium Essentials_ is released under a BSD license.

## Main Concepts ##

### Powerful Selectors

Every element on a page needs a selector so that it can be retrieved for interacting with it or asserting if for 
conditions. _Selenium Essentials_ provides a powerful, yet readable, syntax for building the element selectors.

There are two general kinds of selectors: global and nested.
_Global_ selectors will match elements over the entire page using the specific type of selector. _Nested_ selectors
will limit the search scope to a specific DOM subtree.

```java
// Any element with class page-header will match
Component header = new TextDisplay(global().byClass("page-header"));

// Search only in the DOM subtree rooted by header for the image
Component logo = new Image(nestedWithin(header).byTagName("img"));
```

### Flexible Assertions

Assertions in _Selenium Essentials_ consist of two parts: The condition to assert and the timing. The timing is the
most important aspect for writing tests of dynamic pages. Static content of a page can be tested with _immediate_
assertions where a condition has to be true at the time of invocation. Dynamic content (i.e. loaded via AJAX calls) can
be asserted with _eventual_ assertions that wait for some time for a condition to become true.

Conditions are provided by components. These always apply to the state of a component upon invocation. That way a
condition has to be implemented once, but can be used in both _immediate_ and _eventual_ assertions.

Additionally, by making the timing aspect explicit, readers will immediately spot the fact that dynamic content is 
being tested.

```java
assertThat(immediately(() -> footer.isVisible));

searchButton.click();
assertThat(eventually(() -> searchResults.isVisible()));
```

### Browser Switching

_Selenium  Essentials_ provides rules and annotations for running tests in different browsers. This gives test authors
the chance to test behavior in a specific browser, rather than having to run all tests in the same browser.
       
### Experimental Visual Assertions

Visual assertions allow comparing screenshots of a page or parts thereof against baseline images. This allows a test to
over not only text content of a page, but also to ensure that the visual appearance remains correct over time. Changes
to visual appearance can happen by changes to stylesheets or structural changes. Many of these changes would not be
detected by simply evaluating text content of the page.

While the concept for visual regression testing is not new, _Selenium Essentials_ attempts to make it usable by
providing a very simple API around it. The goal is to be able to formulate a visual assertion in the same readable way
as a regular assertion.

```java
assertThat(immediately(() -> testee.visualAppearanceOf(regionOfInterest)
	.excluding(someUnwantedComponent)
	.isMatchingBaseline("VisualAssertionRuleTest-pageMatching")
));

```

## Development

### Software Requirements

_Selenium Essentials_ uses Gradle as its build system. In order to build the project and run its tests, the following
software has to be installed

* JDK 8
* Mozilla Firefox version 47 or above
* Gecko Driver 0.13 or above (https://github.com/mozilla/geckodriver/releases)
* Google Chrome version 55 or above
* Chrome Driver version 2.27 or above (https://sites.google.com/a/chromium.org/chromedriver/downloads)
* ImageMagick 7.0.5.5 or above (`compare` has to be installed)

The following properties need to be inserted into `gradle.properties` in the `~/.gradle` directory:
* `systemProp.webdriver.gecko.driver` The fully qualified path to the `geckodriver` binary
* `systemProp.webdriver.chrome.driver` The fully qualified path to the `chromedriver` binary

### Branching Model

_Selenium Essentials_ uses the Git Flow branching model. See http://nvie.com/posts/a-successful-git-branching-model/ for
details on Git Flow.

### Building and Testing

The project can be built using `./gradlew build` and tests be executed by `./gradlew test`.

During the tests, Firefox and Chrome will be used for integration testing of the components and rules. If the project is
built in a headless environment, a virtual framebuffer has to be set up for the browsers to work.

## Usage

### Integration

#### Without Existing Selenium Infrastructure

Integration into new projects without an exsiting Selenium testing infrastructrure is very simple. Besides installing
the browser(s) and drivers, authors can almost immediately begin writing tests. Allocation of Selenium `WebDriver`s will
be handled by _Selenium Essentials_.

#### With Existing Selenium Infrastructure

Projects that already have a system for setting up `WebDriver`s can be used with _Selenium Essentials_. In this case the
automatic driver allocation provided by _Selenium Essentials_ has to be prevented from taking place.

`WebtestDriverProvider` provides a `replaceDriverIf` method for conditionally replacing the existing driver. This can
be used to supply _Selenium Essentials_ with the externally created driver for the current thread.

```java
public class SomeWebtest
{
	@Before
	public void useExistingDriver()
	{
		WebDriver existingDriver = ...;
		
		WebtestDriverProvider.replaceDriverIf(essentialsDriver -> essentialsDriver != existingDriver, 
			() -> existingDriver);
	}
	
	@Test
	public void theWebtest()
	{
		...
	}
}
```

## Switching Browsers

_Selenium Essentials_ provides a simple test rule that allows to indicate what browser a test should be run in. By
default, tests are run in Chrome. The browser can be specified per class or per method, with the per method browser
type taking precedence.

```java
public class TestWithDifferentBrowsers
{
	@Rule
	public final @NotNull TestRule browserRule = TargetBrowserRule.withDefaultBrowser(BrowserType.FIREFOX);

	@Test
	@TargetBrowser(BrowserType.CHROME)
	public void thisTestWillRunInChrome()
	{
		...
	}

	@Test
	public void thisTestWillRunFirefox()
	{
		...
	}
}
```
